﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DegerCozumlemeYardimcisi 
{
    public static float ToFloat(this string text){
        float value = 0;
        float.TryParse(text, out value);
        return value;
    }

    public static float ToInt(this string text){
        int value = 0;
        int.TryParse(text, out value);
        return value;
    }
    public static float GetirFaizOndalik(this float yuzdelifaiz){
        return yuzdelifaiz/100;
    }
}
