﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UyariBilgiPaneli : MonoBehaviour
{
    public TextMeshProUGUI _UyariMesaji;

    public void UyariPaneliAc(string uyariMesaji){
        _UyariMesaji.text=uyariMesaji;
        this.gameObject.SetActive(true);
    }
}
