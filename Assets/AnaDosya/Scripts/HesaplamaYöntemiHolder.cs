﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HesaplamaYöntemiHolder : MonoBehaviour
{
   public Yontem SecilenYontem;
   private ParaninZamanDegeriHesaplama paraninZamanDegeriHesaplama;

   private void Start() {
       paraninZamanDegeriHesaplama= GameObject.Find("Controller").GetComponent<ParaninZamanDegeriHesaplama>();
   }
   public enum Yontem{
       
        TyilSonrakiDeger,
        SimdikiDeger,
       
    }

    public void YontemSec(){
        if(SecilenYontem==Yontem.TyilSonrakiDeger)
        {
            paraninZamanDegeriHesaplama.YontemTip=1;
            paraninZamanDegeriHesaplama.Baslik.text="Paranın T Yıl Sonraki Degerini Hesaplama";
        }
        else if(SecilenYontem==Yontem.SimdikiDeger)
        {
            paraninZamanDegeriHesaplama.YontemTip=2;
            paraninZamanDegeriHesaplama.Baslik.text="T Yıl Sonraki Paranın Şimdiki Değerini Hesaplama";
        }
        else
        {
            paraninZamanDegeriHesaplama.YontemTip=0;
        }
        
        paraninZamanDegeriHesaplama.TumGirdileriTemizle();

    }


}


