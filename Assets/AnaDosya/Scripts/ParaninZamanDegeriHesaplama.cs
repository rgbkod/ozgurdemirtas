﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 
using TMPro;


// Yazılım dillerinde türkçe karakterler kullanılmadığı için türkçe karakterler kullanılamaz.
public class ParaninZamanDegeriHesaplama : MonoBehaviour
{
    public InputField _Para;
    public InputField _r; //faiz Orani
    public InputField _T; // T Zaman

    public InputField Sonuc;

    public TextMeshProUGUI SonucDetay;
    public TextMeshProUGUI Baslik;
    public int YontemTip=1;

    public UyariBilgiPaneli _uyariBilgiPaneli;
    public GameObject SonucPaneli;

    public void YontemeGoreDegerHesapla(){
        if(YontemTip==1){
            if(!BosGirdiMi())
                SuankiParaninTZamanSonrakiDegeriHesapla();
        }
        else if(YontemTip==2){
            if(!BosGirdiMi())
                TYilSonrakiParaninBugünküDeğeri();
        }
        else
        {
            // POP UP TO SHOW ERROR
            _uyariBilgiPaneli.UyariPaneliAc("Yanlış değer seçili");
        }
    }


    public void TumGirdileriTemizle(){
        _Para.text="";
        _r.text="";
        _T.text="";
        Sonuc.text="";
        SonucDetay.text="";
        SonucPaneli.SetActive(false);
    }


    private void SuankiParaninTZamanSonrakiDegeriHesapla(){
         float floatSonuc= GirilenPara_FloatDegeri() * TyilFaizGeliri();
         Sonuc.text=floatSonuc.ToString();
         SonucDetay.text= "Şuanki paranın " +_T.text +" zaman sonraki değeri " + floatSonuc.ToString() +" dir";
         SonucPaneli.SetActive(true);
    }

    private void TYilSonrakiParaninBugünküDeğeri(){
         float floatSonuc= GirilenPara_FloatDegeri() / TyilFaizGeliri();
         Sonuc.text=floatSonuc.ToString();
         SonucDetay.text= _T.text +" zaman sonraki paranın şimdiki değeri " + floatSonuc.ToString() +" dir";
         SonucPaneli.SetActive(true);
    }

    private float GirilenPara_FloatDegeri(){
        return _Para.text.ToFloat();
    }
    private float TyilFaizGeliri(){
        return Mathf.Pow((1+_r.text.ToFloat().GetirFaizOndalik()),_T.text.ToInt());
    }

    private bool BosGirdiMi(){
        if(string.IsNullOrEmpty(_Para.text)){
            // _Para girişi yapılmadı
            _uyariBilgiPaneli.UyariPaneliAc("Para girişi yapılmadı");
            return true;
        }
        if(string.IsNullOrEmpty(_r.text)){
            // faiz oran girişi yapılmadı
            _uyariBilgiPaneli.UyariPaneliAc("Faiz oranı girişi yapılmadı");
            return true;
        }
        if(string.IsNullOrEmpty(_T.text)){
            // zaman girişi yapılmadı
             _uyariBilgiPaneli.UyariPaneliAc("Zaman girişi yapılmadı");
            return true;
        }

        return false;
    }

    

}
